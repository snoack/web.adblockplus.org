/**
 * We don't track devices in the EEA
 *
 * Caution: This file exists for backwards compatibility during infrastructure change. It's successor is/will be /js/tracking/tracking.js
 */
document.documentElement.classList.remove('async-hide');
